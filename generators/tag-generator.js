var _ = require('lodash');
var fs = require('fs');
var Promise = require('bluebird');

function run(pipeline, settings) {
    settings.filename = settings.filename || 'collection-decorated';

    return Promise.resolve()
        .then(readTags(pipeline, settings))
        .then(parseTags(pipeline, settings))
        .then(mergeIssues(pipeline, settings))
        .then(decorateConcepts(pipeline, settings));
    // get concepts
    //var concepts = pipeline.getKey('YAMLToJSONResult');
    // read available tags
}

function step(settings) {
    return {
        settings: settings || {},
        run: run
    }
}


function readTags(pipeline) {
    return function() {
        return fs.readdirSync(`${pipeline.getBasePath()}/raw_files/tags`);
    }
}

function parseTags(pipeline) {
    return function(tags) {
        return Promise.mapSeries(tags, (tag) => {
            // read definition
            // read issues
            return {
                tag: 'nrb',
                issues: [{
                    slug: 'aspekt-nrb',
                    fulfilled: true
                }, {
                    slug: 'attribut-coclass',
                    fulfilled: false
                }]
            }
        });
    }
}

function mergeIssues(pipeline) {
    return function(populatedTags) {
        var issues = [];
        _.forEach(populatedTags, (populatedTag) => {
            _.forEach(populatedTag.issues, (issue) => {
                var issue = {
                    tag: populatedTag.tag,
                    slug: issue.slug,
                    fulfilled: issue.fulfilled
                };
                issues.push(issue);
            });
        });
        return issues;
    }
}

function decorateConcepts(pipeline, settings) {
    return function(issues) {
        // get concepts
        var concepts = pipeline.getKey('YAMLToJSONResult');
        return Promise.mapSeries(issues, (issue) => {
            if(issue.fulfilled) {
                _.forEach(concepts, (concept) => {
                    if(concept.slug == issue.slug) {
                        // decorate
                        concept.tags = concept.tags || [];
                        concept.tags.push(issue);
                    }
                });
            }
        })
        .then(function() {
            // all issues ran
            pipeline.setKey('YAMLToJSONResult', concepts);
            pipeline.writeFile({
                path: `${pipeline.getOutputDirPath()}/${settings.filename}.json`,
                data: JSON.stringify(concepts, null, 2),
                title: 'Collection - Decorated'
            })
            return concepts;
        });
    }
}

module.exports.step = step;
module.exports.run = run;
