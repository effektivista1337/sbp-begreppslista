import { Component, Input } from '@angular/core';

@Component({
    selector: 'sbp-concept-card',
    template: `
        <div class="concept-card">
            <h4>{{ concept.concept.sv.term }}<small> - {{concept.concept.sv.source}}</small> <a class="pull-right" style="color: #e1e1e1" href="#/concepts/{{ concept.slug }}"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a></h4>
            
            <div class="row">
                <div class="col-md-6">
                    <p>{{ concept.concept.sv.definition }}</p>

                    <p *ngIf="concept.concept.sv.comment"><strong>Kommentar</strong><br>
                    {{ concept.concept.sv.comment }}</p>
                    <div *ngIf="concept.tags">
                        <span *ngFor="let tag of concept.tags">{{tag.tag}}</span>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-5">
                    <div class="well well-sm">
                        <dl class="dl-horizontal">
                            <dt>Källinformation</dt>
                            <dd><a href="{{ sourceDir }}{{concept.filename}}" target="_blank">Originalfil</a></dd>
                            <dt>Länkar</dt>
                            <dd>
                                <div *ngIf="concept.links?.length > 0">
                                    <div *ngFor="let link of concept.links"><a href="{{ link.url }}">{{ link.name }}</a></div>
                                </div>
                                <div *ngIf="concept.links?.length < 1"><em>Länkar saknas</em></div>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    `
})
export class ConceptCardComponent {
    @Input() concept: Object;
    sourceDir: String;
    constructor() {
        this.sourceDir = 'https://gitlab.com/swe-nrb/sbp-begreppslista/tree/master/raw_files/concepts';
    }
}
