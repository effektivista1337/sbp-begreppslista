import { Component } from '@angular/core';

@Component({
    selector: 'sbp-header',
    template: `
        <h1>SBP Begreppslista</h1>
        <p>Öppen begreppslista skapad av samhällsbyggare för samhällsbyggare</p>
        <ul class="nav nav-pills" style="margin-bottom: 20px">
            <li role="presentation"><a href="#/concepts">Hem</a></li>
            <li role="presentation"><a href="#/propose">Föreslå</a></li>
            <li role="presentation"><a href="#/about">Om</a></li>
            <li role="presentation"><a type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" href="assets/collection.xlsx"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Ladda ner Excel</a></li>
        </ul>
    `
})
export class HeaderComponent {}