// deps
import { NgModule } from '@angular/core';
import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';

// app
import { AppComponent } from './app.component';

// pages
import { ConceptsComponent } from './pages/concepts/concepts.component';
import { ConceptComponent } from './pages/concept/concept.component';
import { ProposeComponent } from './pages/propose/propose.component';
import { AboutComponent } from './pages/about/about.component';

// components
import { HeaderComponent } from './components/header/header.component';
import { ConceptCardComponent } from './components/concept-card/concept-card.component';

// providers
import { ConceptService } from './providers/concept.service';

const appRoutes: Routes = [{
    path: 'concepts',
    component: ConceptsComponent
}, {
    path: 'concepts/:slug',
    component: ConceptComponent
}, {
    path: 'propose',
    component: ProposeComponent
}, {
    path: 'about',
    component: AboutComponent
}, {
    path: '',
    redirectTo: '/concepts',
    pathMatch: 'full'
}];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(appRoutes),
        HttpModule,
        Ng2FilterPipeModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        ConceptsComponent,
        ConceptComponent,
        ConceptCardComponent,
        ProposeComponent,
        AboutComponent
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        ConceptService,
        Location, { provide: LocationStrategy, useClass: HashLocationStrategy }
    ]
})
export class AppModule {}
