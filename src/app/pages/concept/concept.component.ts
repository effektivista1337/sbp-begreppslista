import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConceptService } from '../../providers/concept.service';
import * as _ from 'lodash';

@Component({
    selector: 'sbp-concept',
    template: `
        <a class="btn btn-default" href="#/concepts">Tillbaka</a>
        <sbp-concept-card *ngIf="concept" [concept]="concept"></sbp-concept-card>
        <div id="disqus_thread"></div>
        <script>
        (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://sbp-begreppslista.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    `
})
export class ConceptComponent implements OnInit, OnDestroy {
    concept;
    sub;
    slug;

    constructor(
        private conceptService: ConceptService,
        public route: ActivatedRoute    
        ) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.slug = params['slug'];
            this.conceptService.getConcept(this.slug).subscribe((concept) => this.concept = concept);
            
            // document.disqus_config = function () {
            //   this.page.url = "#/concepts/" + this.slug;
            //   this.page.identifier = this.slug;
            //   this.page.title = this.concept.concept.sv.term + " " + concept.concept.sv.source;
            // };
        });
    }

    ngOnDestroy()  {
        this.sub.unsubscribe();
    }
}
