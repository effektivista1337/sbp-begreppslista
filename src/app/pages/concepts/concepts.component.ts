import { Component } from '@angular/core';
import { ConceptService } from '../../providers/concept.service';
import * as _ from 'lodash';

@Component({
    selector: 'sbp-concepts',
    template: `
        <div class="panel panel-default">
            <div class="panel-body">
                <label for="filterInput">Filter</label>
                <input name="filterInput" [(ngModel)]="conceptFilter.concept.sv.term" class="form-control" placeholder="Filtrera bland {{ concepts?.length }} begrepp...">
            </div>
        </div>
        {{ filterInput }}
        <sbp-concept-card *ngFor="let concept of concepts | filterBy: conceptFilter" [concept]="concept"></sbp-concept-card>
    `
})
export class ConceptsComponent {
    concepts;
    conceptFilter;

    constructor(private conceptService: ConceptService) {
        conceptService.getConcepts().subscribe(list => this.concepts = list);
        this.conceptFilter = { concept: { sv: { term: '' } } };
    }
}
