import { Component } from '@angular/core';
import * as  _ from 'lodash';
import * as yaml from 'js-yaml';
import { UUID } from 'angular2-uuid';
import * as slug from 'slug';

@Component({
    selector: 'sbp-propose',
    template: require('./propose.component.html')
})
export class ProposeComponent {
    input = {
        sv: {
            term: "",
            source: "",
            definition: "",
            comment: ""
        },
        en: {
            term: "",
            source: "",
            definition: "",
            comment: ""
        }
    };
    languages: any[];
    links: any[];
    yaml: String;
    obj;
    /**
     *
     */
    constructor() {
        this.languages = ["en"]
        this.links = [{
            name: "",
            url: ""
        }];
    }

    addLink() {
        this.links.push({
            name: "",
            url: ""
        });
    }

    deleteLink(link) {
        this.links.splice(link, 1);
    }

    build() {
        let generatedSlug = (slug(this.input.sv.term + '-' + this.input.sv.source)).toLowerCase();
        this.obj = {
            _v: 1,
            id: UUID.UUID(),
            slug: generatedSlug,
            filename: (generatedSlug + '.yml'),
            concept: {
                sv: { 
                    term: this.input.sv.term,
                    source: this.input.sv.source,
                    definition: this.input.sv.definition,
                    comment: this.input.sv.comment
                },
                en: { 
                    term: this.input.en.term,
                    source: this.input.en.source,
                    definition: this.input.en.definition,
                    comment: this.input.en.comment
                }
            },
            links: this.links
        };
        this.yaml = yaml.dump(this.obj);
    }
    download($event) {
        var blob = new Blob([this.yaml], { type: 'text/plain' });
        var url= window.URL.createObjectURL(blob);
        let a = document.createElement("a");
        a.href = url;
        a.download = this.obj.filename;
        a.click();
        window.URL.revokeObjectURL(url);
    }
}
