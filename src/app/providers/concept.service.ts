import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import * as _ from "lodash";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ConceptService {
    
    private conceptsUrl;
    private concepts: any[] = [];

    constructor(private http: Http) {
        this.conceptsUrl = '/assets/collection.json';
    }

    getConcepts() {
        return this
            .http
            .get(this.conceptsUrl)
            .map(this.extractData);
    }

    getConcept(slug: string) {
        return this
            .http
            .get(this.conceptsUrl)
            .map((res: Response) => {
                let body = res.json();
                 return _.find(body, (concept:any) => {
                    return concept.slug == slug;
                });
            });
    }

    private extractData(res: Response) {
        let body = res.json();

        // sort list
        body = _.sortBy(body, 'concept.sv.term');

        this.concepts = body;

        return this.concepts;
    }
}