import { Component } from '@angular/core';

@Component({
    selector: 'sbp-app',
    template: ` 
    <div class="container">
        <div class="col-md-12">
            <sbp-header></sbp-header>
            <router-outlet></router-outlet>
        </div>
    </div>
    `
})
export class AppComponent {}