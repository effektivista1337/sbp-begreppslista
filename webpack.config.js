var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: './src/app/main.ts',
  output: {
    path: path.resolve(__dirname, 'public'),
    //publicPath: '/sbp-begreppslista',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts-loader'
      }, {
        "test": /\.html$/,
        "loader": "raw-loader"
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.ts']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new CopyWebpackPlugin([{
      from: 'output_files',
      to: 'assets'
    }])
  ]
};
