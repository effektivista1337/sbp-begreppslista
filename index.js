var infopack = require('infopack');
var markupGenerator = require('sbp-markup-generator-official');
var ymlToJson = require('sbp-utils-yml-to-json');

var jsonToXlsx = require('infopack-gen-json-to-xlsx');
var tagGenerator = require('./generators/tag-generator');

var pipeline = new infopack.Pipeline();

// generate json file from yamls
pipeline.addStep(ymlToJson.step({
	omitFile: true,
	readPath: './raw_files/concepts'
}));

pipeline.addStep(tagGenerator.step({
	filename: 'collection'
}));

/*
generator needs to be upgraded
// generate markdown file from json
pipeline.addStep(markupGenerator.step({
	readPath: './work_folder/collection.json',
	templatePath: './support_files/dokument.md'
}));
*/

// generate excel file from json
pipeline.addStep(jsonToXlsx.step({
	storageKey: 'YAMLToJSONResult',
	mapper: (conceptObj) => {
		return {
			"Term": conceptObj.concept.sv.term,
			"Källa": conceptObj.concept.sv.source,
			"Definition": conceptObj.concept.sv.definition
		};
	}
}));

// run pipeline
pipeline.run()
	.then(function(data) {
		console.log(pipeline.prettyTable());
	});
