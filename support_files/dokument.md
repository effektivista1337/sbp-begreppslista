# Begreppslista

Detta dokument innehåller av branschen definierade begrepp.
{{#each data}}

## {{concept.sv.term}} - {{concept.sv.source}}

{{#if concept.en}}
Översättning: {{concept.en.term}}  
{{/if}}

{{concept.sv.definition}}

### Länkar

{{#if links}}
{{#each links}}
* [{{name}}]({{url}})
{{/each}}
{{else}}
_Detta begrepp saknar länkar_
{{/if}}

{{/each}}
